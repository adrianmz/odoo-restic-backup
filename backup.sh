#!/bin/bash

# Check required variables are set

if [[ -z ${ODOO_ADMIN_PASSWORD} ]]; then
    echo 'Missing ODOO_ADMIN_PASSWORD, exiting process...'
    exit 1
fi

if [[ -z ${REPO_URL} ]]; then
    echo 'Missing REPO_URL, exiting process...'
    exit 1
fi

if [[ -z ${REPO_NAME} ]]; then
    echo 'Missing REPO_NAME, exiting process...'
    exit 1
fi

if [[ -z ${AWS_ACCESS_KEY_ID} ]]; then
    echo 'Missing AWS_ACCESS_KEY_ID, exiting process...'
    exit 1
fi

if [[ -z ${AWS_SECRET_ACCESS_KEY} ]]; then
    echo 'Missing AWS_SECRET_ACCESS_KEY, exiting process...'
    exit 1
fi


# Create a backup
curl -NS -X POST \
    -F "master_pwd=${ODOO_ADMIN_PASSWORD}" \
    -F "name=${ODOO_DATABASE:=odoo}" \
    -F "backup_format=zip" \
    -o ./backup.zip \
    --verbose --ipv4 \
    ${ODOO_URL:="http://odoo:8069"}/web/database/backup


# Send backup to remote storage and delete the local copy
restic -r s3:${REPO_URL}/${REPO_NAME} ./backup.zip
rm backup.zip

# If keep policy is set, apply to restic repo
if [[ -v KEEP_POLICY ]]; then
    restic -r s3:${REPO_URL}/${REPO_NAME} forget --prune ${KEEP_POLICY}
fi