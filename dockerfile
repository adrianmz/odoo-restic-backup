FROM restic/restic:latest

COPY backup.sh ./backup/backup.sh
COPY crontab /etc/crontabs/root

RUN apk --no-cache add curl

ENTRYPOINT []
CMD ["crond", "-f", "-d", "8"]
