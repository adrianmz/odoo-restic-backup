# Restic Odoo Backups

Docker container to automate the backup of an odoo server with restic into S3 compatible remote storage 

## Variables
- ODOO_URL: Url of the odoo instance, if the odoo instance is in the same docker network the service name can be used. (Default: http://odoo:8069)
- ODOO_DATABASE: Name of the odoo database to backup. (Default: odoo)
- ODOO_ADMIN_PASSWORD: Admin pasword of the odoo database manager. If no password is set, the backup will not work (Default: none) 
- REPO_URL: Url of the object storage. (Required)
- REPO_NAME: Name of the restic repository where the backup will be stored (Default: odoo)
- AWS_ACCESS_KEY_ID: Access key id of your S3 storage account
- AWS_SECRET_ACCESS_KEY: Secret Access key of your S3 storage account
- KEEP_POLICY: Restic policy to forget old snapshots. (For example: "--keep-within-daily 14d --keep-within-weekly 1m --keep-within-monthly 1y --keep-within-yearly 75y
")

## Compatible storage
- **Backblaze (recommended)**
- Amazon S3
- Minio
- Wasabi

